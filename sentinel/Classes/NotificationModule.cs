﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Mail;

namespace sentinel
{

    /// <summary>
    /// This module is responsible for sending out a text to the patient's care taker, notifying them.
    /// of the incident.
    /// </summary>
    class NotificationModule
    {
        // Contact number for the care taker
        String contactNumber = "5615040623@tmomail.net";

        /// <summary>
        /// Sends out a text notifying the care taker that their loved one might be in trouble 
        /// </summary>
        /// <param name="msg"></param>
        public void sendNotification(String msg)
        {

            // Set up the message object 
            System.Net.Mail.MailMessage message = new System.Net.Mail.MailMessage();
            message.To.Add(contactNumber);
            message.Subject = "";
            message.From = new System.Net.Mail.MailAddress("info@bocamusicschool.com");
            message.Body = msg;
            message.Priority = MailPriority.High;

            // configures the smpt client object 
            System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient();
            smtp.Credentials = new System.Net.NetworkCredential("jassis@fau.edu", "chaves2401");
            smtp.Port = 587;
            smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
            smtp.Host = "smtp.gmail.com";
            smtp.EnableSsl = true;

            // Sends out the text 
            smtp.Send(message);
        }

    }
}
