﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.IO.Ports;

namespace sentinel
{
    public class KinectMobilePositionModule
    {

        /// <summary>
        /// This is the workspace resolution 
        /// </summary>
        private int width = 640; // The width of the virtual screen
        private int height = 480; // The height of the virtual screen 
        private int middleOfScreen = 320;
        private int safeZone = 100; // The amount of pixels to the right and left of the middle of the screen when the kinect won't move 
        private double engFactor = 1.2; // David's engineering factor. Modify this to account for friction, drag, etc. 
        private DateTime lastSent = DateTime.Now; // Keeps track of the last time a signal was sent to the Arduino
        private int transmitWait = 750; // How many milliseconds the program should wait until the next transmition to the Arduino
        private SerialPort port;
        private Boolean isError = false;



        public KinectMobilePositionModule(int screenWidth, int screenHeight)
        {
            try
            {
                // Updates the values from the virtual workspace
                width = screenWidth;
                height = screenHeight;
                middleOfScreen = width / 2; // Calculates the value for the middle of the screen

                // Create a bluetooth connection to the Arduino
                port = new SerialPort("COM4", 9600, Parity.None, 8, StopBits.One);
                port.Open();
            }
            catch (Exception)
            {

                isError = true; // flags the error 
            }
            

        }

        //Destructor that closes 
        ~KinectMobilePositionModule()
        {
            port.Close();
        }

        public void UpdateSkelPosition(Point skelPosition)
        {

            if (!isError)
            {

                try
                {
                    // Calculates the difference between the current time and the last time a signal was sent to the Arduino
                    TimeSpan timeEllapsed = DateTime.Now - lastSent;

                    // Limits the amount of time the program sends a signal to the Arduino 
                    if (timeEllapsed.Milliseconds > transmitWait)
                    {

                        // Checks if the current user position is on the right or left side of the screen 
                        if (skelPosition.X < GetLeftBoundary())
                        {

                            // The difference between the middle of the screen and the user's position is used as the power scaler for the MoveLeft function 
                            MoveLeft((int)(middleOfScreen - skelPosition.X));

                        }
                        else if (skelPosition.X > GetRightBundary())
                        {
                            // Calculates how close to the right edge the user is and sends the value to the MoveRight function as a power scaler
                            MoveRight((int)(255));//width - skelPosition.X - 100));
                        }
                        else
                            STOP_MOTOR();
                        

                        lastSent = DateTime.Now; // Sets the variable with the current time
                    }
                }
                catch (Exception)
                {

                    throw;
                }

            }
        }

        /// <summary>
        /// Sends a signal to the motors to move the kinect left. The scaler sets the power in which the motors should rotate
        /// </summary>
        /// <param name="scaler"></param>
        private void MoveLeft(int scaler)
        {
            // moves the kinect left 
            port.WriteLine("forward " + ConvertScalerToSerialValue(scaler) + ".");
        }

        /// <summary>
        /// Sends a signal to the motors to move the kinect right. The scaler sests the power in which the motors should rotate 
        /// </summary>
        /// <param name="scaler"></param>
        private void MoveRight(int scaler)
        {
            // Moves kinect right 
            port.WriteLine("reverse " + ConvertScalerToSerialValue(scaler) + ".");
        }

        public void STOP_MOTOR()
        {
            port.WriteLine("break.");
        }

        /// <summary>
        /// This converts the value that the kinect was using (0 - 320) to the values that the Arduino understands (0 - 255)
        /// </summary>
        /// <param name="scaler"></param>
        /// <returns></returns>
        private int ConvertScalerToSerialValue(int scaler)
        {

            // scaler - The calculated value from the virtual workspace
            // middleOfScreen - The value for the center of the screen in the X axis
            // engFactor - This is the multiplier to account for any other parameters that we didn't account for, such as friction, etc. 
            int returnScaler = (int)((double)((double)scaler / (double)middleOfScreen) * (double)engFactor * (double)255);

            // Motor operating range is 107 - 255
            if (returnScaler < 150)
                return 200;
            else if (returnScaler > 255)
                return 255;
            else
                return returnScaler;

        }

        // Returns the boundary verical coordinate 
        public int GetLeftBoundary()
        {
            return middleOfScreen - safeZone;
        }

        // Display the boudndary vertical coordinate 
        public int GetRightBundary()
        {
            return middleOfScreen + safeZone; 
        }
    }
}
