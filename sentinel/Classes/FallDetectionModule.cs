﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace sentinel
{

    class FallDetectionModule
    {

        // Holds the floor plane values, and point values 
        private PositioningCoord positionCoordinate = new PositioningCoord();

        // Keeps a record of the joint's last position
        private Point lastPosition;

        // fall magnitude 
        private double depthMag = 0;

        // Stops the tracking of the joint 
        public bool StopTracking = false;

        /// <summary>
        /// Tracks the trajectory of the joint. It enables the class to know if the user might be falling or not.
        /// </summary>
        /// <param name="jointScreenPosition"></param>
        public void trackUserNeckPosition(Point jointScreenPosition)
        {

            // Temporary variable that holds the difference between the last known position and current  
            double diffCalc = lastPosition.Y - jointScreenPosition.Y;

            // If the difference between the last joint position and the current joint position is 
            // negative, it indicates that the join is falling otherwise, we know that the node is not 
            // falling, therefore we reset the variable depthMag
            if(!StopTracking)
                depthMag += diffCalc;

            // Stores the current position as the last position 
            lastPosition = jointScreenPosition;

        }

        // Returns whether or not the user is likely to have fallen 
        public bool isFall()
        {

            return positionCoordinate.isFall();

        }

        /// <summary>
        /// Stores the floor position values
        /// </summary>
        /// <param name="fp"></param>
        public void SetFloorPlaneValues(PositioningCoord fp)
        {
            positionCoordinate.A = fp.A;
            positionCoordinate.B = fp.B;
            positionCoordinate.C = fp.C;
            positionCoordinate.D = fp.D;
        }

        /// <summary>
        /// Stores the joint position values 
        /// </summary>
        /// <param name="jp"></param>
        public void setJointPosition(PositioningCoord jp, DateTime jointTimeStamp)
        {
            positionCoordinate.setJointPosition(jp.X, jp.Y, jp.Z, jointTimeStamp);
        }
    }

    /// <summary>
    /// This is used to store the values for the plane equation Ax + By + Cz + D = 0
    /// </summary>
    class PositioningCoord
    {
        // Functional Parameters
        float minTorsoHeight = (float)1;

        // Flooat Plane Values
        public float A = 0;
        public float B = 0;
        public float C = 0;
        public float D = 0;

        // point in space 
        public float X = 3;
        public float Y = 3;
        public float Z = 3;

        // Current and Previous time stamp for the joint 
        DateTime currentTime = DateTime.Now;
        DateTime previousTime = DateTime.Now.AddMinutes(-1);

        // privious Distance
        float currentDistance = 0;
        float previousDistance = 1;

        /// <summary>
        /// Gets the distance between the joint and the floorplane 
        /// </summary>
        /// <returns></returns>
        public float getFloorDistance()
        {
            // Returns the current distance 
            return (float)(A * X + B * Y + C * Z + D);
        }

        /// <summary>
        /// Updates the joint coordinate
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="z"></param>
        /// <param name="currentTime"></param>
        public void setJointPosition(float x, float y, float z, DateTime curTime)
        {
            // Sets coordinates
            X = x;
            Y = y;
            Z = z;

            // Updates the distance and timestamp every 300 milliseconds 
            TimeSpan ts = curTime - previousTime;
            if (ts.Milliseconds > 300)
            {

                // updates the previous time value
                previousTime = currentTime;

                // Updates the current time 
                currentTime = curTime;
                
                // Updates the previous distance value
                previousDistance = currentDistance;

                // Calculate the current distance value 
                currentDistance = getFloorDistance();

            }
        }

        /// <summary>
        ///  Gets the speend in which the user traveled in 1/3 of a second 
        /// </summary>
        /// <returns></returns>
        private float GetSpeed()
        {

            // Gets the timespan between the two timestamps
            TimeSpan ts = currentTime - previousTime;

            // Calculates the speed in millimeters 
            float distanceMilli = (currentDistance - previousDistance) * 1000;

            // calculates the the speed in millimeters/milliseconds
            return (float)( distanceMilli / ts.TotalMilliseconds);

        }

        public Boolean isFall()
        {

            // Checks if the user's neck is below 1 meter, and the speed in which the user got there
            if (getFloorDistance() < minTorsoHeight)
                return true;
            else
                return false;

        }
    }
}
