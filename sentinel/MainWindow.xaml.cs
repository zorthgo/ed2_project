﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Threading;
using Microsoft.Kinect;
using Microsoft.Speech.AudioFormat;
using Microsoft.Speech.Recognition;
using Microsoft.Speech.Synthesis;
using System.IO;


namespace sentinel
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        String patientName = "Jairo";
        bool waitingResponse = false; // Determines whether the system is waiting for a response from the user 
        string CurrentQuestion = "";

        bool pauseListening = false;

        /// Creates an instance of your kinect sensor
        KinectSensor sensor;

        // Sends data to the motors sot they can move
        sentinel.KinectMobilePositionModule mobilePositioning = new sentinel.KinectMobilePositionModule(640, 480);

        // Instantiate the fall detection module 
        sentinel.FallDetectionModule fallDetect = new sentinel.FallDetectionModule();

        // Creates an instance of the Recognition engine
        private SpeechRecognitionEngine speechRecognizer;
        private static RecognizerInfo GetKinectRecognizer()
        {
          
            Func<RecognizerInfo, bool> matchingFunc = r =>
            {
                string value;
                r.AdditionalInfo.TryGetValue("Kinect", out value);
                return "True".Equals(value, StringComparison.InvariantCultureIgnoreCase) && "en-US".Equals(r.Culture.Name, StringComparison.InvariantCultureIgnoreCase);
            };

            return SpeechRecognitionEngine.InstalledRecognizers().Where(matchingFunc).FirstOrDefault();
        }

        /// <summary>
        /// Keeps track of when the sensor has to move
        /// </summary>
        static Int32 AddSensorAngle = 0;
        static DateTime lastTilt = DateTime.Now; // Keeps track of when the kinect moved last 

        private Point User_Screen_Position;

        /// <summary>
        /// Thickness of body center ellipse
        /// </summary>
        private const double BodyCenterThickness = 10;

        /// <summary>
        /// Thickness of clip edge rectangles
        /// </summary>
        private const double ClipBoundsThickness = 10;

        /// <summary>
        /// Brush used to draw skeleton center point
        /// </summary>
        private readonly Brush centerPointBrush = Brushes.Blue;

        /// <summary>
        /// Brush used for drawing joints that are currently tracked
        /// </summary>
        private readonly Brush trackedJointBrush = new SolidColorBrush(Color.FromArgb(255, 68, 192, 68));

        /// <summary>
        /// Brush used for drawing joints that are currently inferred
        /// </summary>       
        private readonly Brush inferredJointBrush = Brushes.Yellow;

        /// <summary>
        /// Pen used for drawing bones that are currently tracked
        /// </summary>
        private readonly Pen trackedBonePen = new Pen(Brushes.Green, 6);

        /// <summary>
        /// Pen used for drawing bones that are currently inferred
        /// </summary>       
        private readonly Pen inferredBonePen = new Pen(Brushes.Gray, 1);

        /// <summary>
        /// Drawing image that we will display
        /// </summary>
        private DrawingImage imageSource;

        /// <summary>
        /// Thickness of drawn joint lines
        /// </summary>
        private const double JointThickness = 3;


        /// <summary>
        /// Drawing group for skeleton rendering output
        /// </summary>
        private DrawingGroup drawingGroup;
        /// <summary>
        /// Width of output drawing
        /// </summary>
        private const float RenderWidth = 640.0f;

        /// <summary>
        /// Height of our output drawing
        /// </summary>
        private const float RenderHeight = 480.0f;

        public MainWindow()
        {
            InitializeComponent();
            //After Initialization subscribe to the loaded event of the form
            Loaded += MainWindow_Loaded;

            //After Initialization subscribe to the unloaded event of the form
            //We use this event to stop the sensor when the application is being closed.
            Unloaded += MainWindow_Unloaded;

            // Displays the safe zone on the screen 
            LeftBoundry.X1 = mobilePositioning.GetLeftBoundary();
            LeftBoundry.X2 = mobilePositioning.GetLeftBoundary();
            RightBoundry.X1 = mobilePositioning.GetRightBundary();
            RightBoundry.X2 = mobilePositioning.GetRightBundary();
        }


        void MainWindow_Unloaded(object sender, RoutedEventArgs e)
        {
            //stop the Sestor
            sensor.Stop();

        }


        void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {

            if (KinectSensor.KinectSensors.Count > 0)
                sensor = KinectSensor.KinectSensors.FirstOrDefault(s => s.Status == KinectStatus.Connected); // Gets the available sensor
            else
            {
                Message.Text = "Kinect Sensor is not Found";
                Message.Background = new SolidColorBrush(Colors.Orange);
                Message.Foreground = new SolidColorBrush(Colors.Black);
            }


            speechRecognizer = CreateSpeechRecognizer();

            //Create a Drawing Group that will be used for Drawing
            this.drawingGroup = new DrawingGroup();

            //Create an image Source that will display our skeleton
            this.imageSource = new DrawingImage(this.drawingGroup);

            //Display the Image in our Image control
            UserImage.Source = imageSource;
            

            try
            {
                //Check if the Sensor is Connected
                if (sensor.Status == KinectStatus.Connected)
                {
                    //Start the Sensor
                    sensor.Start();

                    //Tell Kinect Sensor to use the Default Mode(Human Skeleton Standing) || Seated(Human Skeleton Sitting Down)
                    sensor.SkeletonStream.TrackingMode = SkeletonTrackingMode.Default;
                    //Subscribe to te  Sensor's SkeletonFrameready event to track the joins and create the joins to display on our image control
                    sensor.SkeletonFrameReady += sensor_SkeletonFrameReady;

                    //nice message with Colors to alert you if your sensor is working or not
                    Message.Text = "Sentinel Fully Operational";
                    Message.Background = new SolidColorBrush(Colors.Green);
                    Message.Foreground = new SolidColorBrush(Colors.White);

                    sensor.ElevationAngle = 0;

                    // Turn on the skeleton stream to receive skeleton frames
                    this.sensor.SkeletonStream.Enable();

                    Start();

                }
                else if (sensor.Status == KinectStatus.Disconnected)
                {
                    //nice message with Colors to alert you if your sensor is working or not
                    Message.Text = "Kinect Sensor is not Connected";
                    Message.Background = new SolidColorBrush(Colors.Orange);
                    Message.Foreground = new SolidColorBrush(Colors.Black);

                }
                else if (sensor.Status == KinectStatus.NotPowered)
                {
                    //nice message with Colors to alert you if your sensor is working or not
                    Message.Text = "Kinect Sensor is not Powered";
                    Message.Background = new SolidColorBrush(Colors.Red);
                    Message.Foreground = new SolidColorBrush(Colors.Black);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);

            }

            SpeakThis("Sentinel has been activated!");

        }

        private void Start()
        {
            //set sensor audio source to variable
            var audioSource = sensor.AudioSource;
            //Set the beam angle mode - the direction the audio beam is pointing
            //we want it to be set to adaptive
            audioSource.BeamAngleMode = BeamAngleMode.Adaptive;
            //start the audiosource 

            var kinectStream = audioSource.Start();
            //configure incoming audio stream
            speechRecognizer.SetInputToAudioStream(
                kinectStream, new SpeechAudioFormatInfo(EncodingFormat.Pcm, 16000, 16, 1, 32000, 2, null));
            //make sure the recognizer does not stop after completing     
            speechRecognizer.RecognizeAsync(RecognizeMode.Multiple);
            //reduce background and ambient noise for better accuracy
            sensor.AudioSource.EchoCancellationMode = EchoCancellationMode.None;
            sensor.AudioSource.AutomaticGainControlEnabled = false;
        }

        private SpeechRecognitionEngine CreateSpeechRecognizer()
        {
            //set recognizer info
            RecognizerInfo ri = GetKinectRecognizer();
            //create instance of SRE
            SpeechRecognitionEngine sre;
            sre = new SpeechRecognitionEngine(ri.Id);

            //Now we need to add the words we want our program to recognise
            var grammar = new Choices();
            grammar.Add("hello");
            grammar.Add("goodbye");
            grammar.Add("Yes");
            grammar.Add("No");
            grammar.Add("die");

            //set culture - language, country/region
            var gb = new GrammarBuilder { Culture = ri.Culture };
            gb.Append(grammar);

            //set up the grammar builder
            var g = new Grammar(gb);
            sre.LoadGrammar(g);

            //Set events for recognizing, hypothesising and rejecting speech
            sre.SpeechRecognized += SreSpeechRecognized;
            sre.SpeechHypothesized += SreSpeechHypothesized;
            sre.SpeechRecognitionRejected += SreSpeechRecognitionRejected;
            return sre;
        }

        private void SreSpeechRecognitionRejected(object sender, SpeechRecognitionRejectedEventArgs e)
        {
            RejectSpeech(e.Result);

        }

        private void RejectSpeech(RecognitionResult recognitionResult)
        {
            if (!pauseListening)
                MSG2.Text = "Pardon Moi?";
        }

        private void SreSpeechHypothesized(object sender, SpeechHypothesizedEventArgs e)
        {
            if (!pauseListening)
                MSG2.Text = "Hypothesized: " + e.Result.Text + " " + e.Result.Confidence;
        }

        private void SreSpeechRecognized(object sender, SpeechRecognizedEventArgs e)
        {
            if (!pauseListening)
            {
                //Very important! - change this value to adjust accuracy - the higher the value
                //the more accurate it will have to be, lower it if it is not recognizing you
                if (e.Result.Confidence < .7)
                {
                    RejectSpeech(e.Result);
                }
                //and finally, here we set what we want to happen when 
                //the SRE recognizes a word
                switch (e.Result.Text.ToUpperInvariant())
                {
                    case "HELLO":
                        MSG2.Text = "Hi there.";
                        break;
                    case "GOODBYE":
                        MSG2.Text = "Goodbye then.";
                        break;
                    case "YES":
                        MSG2.Text = "YES";
                        QuestionResponse("YES");
                        break;
                    case "NO":
                        MSG2.Text = "NO";
                        QuestionResponse("NO");
                        break;
                    case "DIE":
                        mobilePositioning.STOP_MOTOR();
                        Application.Current.Shutdown();
                        break;
                    default:
                        break;
                }
            }
        }

        private void QuestionResponse(string resp)
        {

            switch (CurrentQuestion)
            {
                case "isFall":
                    isFallResponse(resp);
                    break;
                default:
                    break;
            }

        }

        /// <summary>
        //When the Skeleton is Ready it must draw the Skeleton
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void sensor_SkeletonFrameReady(object sender, SkeletonFrameReadyEventArgs e)
        {

            
            //declare an array of Skeletons
            Skeleton[] skeletons = new Skeleton[1];

            //Opens a SkeletonFrame object, which contains one frame of skeleton data.
            using (SkeletonFrame skeletonframe = e.OpenSkeletonFrame())
            {
                //Check if the Frame is Indeed open
                if (skeletonframe != null)
                {

                    // Gets the floor plane equation from the skeleton frame 
                    sentinel.PositioningCoord floorPlane = new PositioningCoord();
                    floorPlane.A = skeletonframe.FloorClipPlane.Item1;
                    floorPlane.B = skeletonframe.FloorClipPlane.Item2;
                    floorPlane.C = skeletonframe.FloorClipPlane.Item3;
                    floorPlane.D = skeletonframe.FloorClipPlane.Item4;

                    // Passes the floor plane values to the FallDetectionModule
                    fallDetect.SetFloorPlaneValues(floorPlane);

                    skeletons = new Skeleton[skeletonframe.SkeletonArrayLength];

                    // Copies skeleton data to an array of Skeletons, where each Skeleton contains a collection of the joints.
                    skeletonframe.CopySkeletonDataTo(skeletons);

                    //draw the Skeleton based on the Default Mode(Standing), "Seated"
                    if (sensor.SkeletonStream.TrackingMode == SkeletonTrackingMode.Default)
                    {
                        //Draw standing Skeleton
                        DrawStandingSkeletons(skeletons);
                        
                    }
                    else if (sensor.SkeletonStream.TrackingMode == SkeletonTrackingMode.Seated)
                    {
                        //Draw a Seated Skeleton with 10 joints
                        DrawSeatedSkeletons(skeletons);
                    }
                }

            }


        }



        //Thi Function Draws the Standing  or Default Skeleton
        private void DrawStandingSkeletons(Skeleton[] skeletons)
        {

            using (DrawingContext dc = this.drawingGroup.Open())
            {
                //Draw a Transparent background to set the render size or our Canvas
                dc.DrawRectangle(Brushes.Black, null, new Rect(0.0, 0.0, RenderWidth, RenderHeight));

                //If the skeleton Array has items
                if (skeletons.Length != 0)
                {
                    //Loop through the Skeleton joins
                    foreach (Skeleton skel in skeletons)
                    {
                      
                        RenderClippedEdges(skel, dc);

                        if (skel.TrackingState == SkeletonTrackingState.Tracked)
                        {
                            this.DrawBonesAndJoints(skel, dc);


                        }
                        else if (skel.TrackingState == SkeletonTrackingState.PositionOnly)
                        {
                            dc.DrawEllipse(this.centerPointBrush,
                                           null,
                                          this.SkeletonPointToScreen(skel.Position), BodyCenterThickness, BodyCenterThickness);

                        }

                    }


                }

                //Prevent Drawing outside the canvas
                this.drawingGroup.ClipGeometry = new RectangleGeometry(new Rect(0.0, 0.0, RenderWidth, RenderHeight));

            }
        }


        private void DrawSeatedSkeletons(Skeleton[] skeletons)
        {

            using (DrawingContext dc = this.drawingGroup.Open())
            {
                //Draw a Transparent background to set the render size
                dc.DrawRectangle(Brushes.Black, null, new Rect(0.0, 0.0, RenderWidth, RenderHeight));

                if (skeletons.Length != 0)
                {
                    foreach (Skeleton skel in skeletons)
                    {
                        RenderClippedEdges(skel, dc);

                        if (skel.TrackingState == SkeletonTrackingState.Tracked)
                        {
                            this.DrawBonesAndJoints(skel, dc);


                        }
                        else if (skel.TrackingState == SkeletonTrackingState.PositionOnly)
                        {
                            dc.DrawEllipse(this.centerPointBrush, null, this.SkeletonPointToScreen(skel.Position), BodyCenterThickness, BodyCenterThickness);

                        }

                    }


                }

                //Prevent Drawing outside the canvas
                this.drawingGroup.ClipGeometry = new RectangleGeometry(new Rect(0.0, 0.0, RenderWidth, RenderHeight));

            }
        }


        /// <summary>
        /// Draws indicators to show which edges are clipping skeleton data
        /// </summary>
        /// <param name="skeleton">skeleton to draw clipping information for</param>
        /// <param name="drawingContext">drawing context to draw to</param>
        private static void RenderClippedEdges(Skeleton skeleton, DrawingContext drawingContext)
        {
            TimeSpan timeEllapsed = DateTime.Now - lastTilt;

            if (skeleton.ClippedEdges.HasFlag(FrameEdges.Bottom))
            {

                drawingContext.DrawRectangle(
                    Brushes.Red,
                    null,
                    new Rect(0, RenderHeight - ClipBoundsThickness, RenderWidth, ClipBoundsThickness));

                if (timeEllapsed.Seconds > 1)
                    if (AddSensorAngle > -25)
                        AddSensorAngle -= 1;

            }

            if (skeleton.ClippedEdges.HasFlag(FrameEdges.Top))
            {
                drawingContext.DrawRectangle(
                    Brushes.Red,
                    null,
                    new Rect(0, 0, RenderWidth, ClipBoundsThickness));

                if (timeEllapsed.Seconds > 1)
                    if (AddSensorAngle < 25)
                        AddSensorAngle += 1;
            }

            if (skeleton.ClippedEdges.HasFlag(FrameEdges.Left))
            {
                drawingContext.DrawRectangle(
                    Brushes.Red,
                    null,
                    new Rect(0, 0, ClipBoundsThickness, RenderHeight));
            }

            if (skeleton.ClippedEdges.HasFlag(FrameEdges.Right))
            {
                drawingContext.DrawRectangle(
                    Brushes.Red,
                    null,
                    new Rect(RenderWidth - ClipBoundsThickness, 0, ClipBoundsThickness, RenderHeight));
            }
        }

        /// <summary>
        /// Draws a skeleton's bones and joints
        /// </summary>
        /// <param name="skeleton">skeleton to draw</param>
        /// <param name="drawingContext">drawing context to draw to</param>
        private void DrawBonesAndJoints(Skeleton skeleton, DrawingContext drawingContext)
        {

            // Render Torso
            this.DrawBone(skeleton, drawingContext, JointType.Head, JointType.ShoulderCenter);
            this.DrawBone(skeleton, drawingContext, JointType.ShoulderCenter, JointType.ShoulderLeft);
            this.DrawBone(skeleton, drawingContext, JointType.ShoulderCenter, JointType.ShoulderRight);
            this.DrawBone(skeleton, drawingContext, JointType.ShoulderCenter, JointType.Spine);
            this.DrawBone(skeleton, drawingContext, JointType.Spine, JointType.HipCenter);
            this.DrawBone(skeleton, drawingContext, JointType.HipCenter, JointType.HipLeft);
            this.DrawBone(skeleton, drawingContext, JointType.HipCenter, JointType.HipRight);

            // Left Arm
            this.DrawBone(skeleton, drawingContext, JointType.ShoulderLeft, JointType.ElbowLeft);
            this.DrawBone(skeleton, drawingContext, JointType.ElbowLeft, JointType.WristLeft);
            this.DrawBone(skeleton, drawingContext, JointType.WristLeft, JointType.HandLeft);


            // Right Arm
            this.DrawBone(skeleton, drawingContext, JointType.ShoulderRight, JointType.ElbowRight);
            this.DrawBone(skeleton, drawingContext, JointType.ElbowRight, JointType.WristRight);
            this.DrawBone(skeleton, drawingContext, JointType.WristRight, JointType.HandRight);

            // Left Leg
            this.DrawBone(skeleton, drawingContext, JointType.HipLeft, JointType.KneeLeft);
            this.DrawBone(skeleton, drawingContext, JointType.KneeLeft, JointType.AnkleLeft);
            this.DrawBone(skeleton, drawingContext, JointType.AnkleLeft, JointType.FootLeft);

            // Right Leg
            this.DrawBone(skeleton, drawingContext, JointType.HipRight, JointType.KneeRight);
            this.DrawBone(skeleton, drawingContext, JointType.KneeRight, JointType.AnkleRight);
            this.DrawBone(skeleton, drawingContext, JointType.AnkleRight, JointType.FootRight);

            // Tracks how many bones are successfully tracked
            double trackedBones = 0;
            double totalBones = 0;

            // Render Joints
            foreach (Joint joint in skeleton.Joints)
            {
                Brush drawBrush = null;
                totalBones += 1; // Tracks each bone 

                if (joint.TrackingState == JointTrackingState.Tracked)
                {
                    drawBrush = this.trackedJointBrush;
                    trackedBones += 1;
                }
                else if (joint.TrackingState == JointTrackingState.Inferred)
                {

                    drawBrush = this.inferredJointBrush;

                }

                if (drawBrush != null)
                {
                    drawingContext.DrawEllipse(drawBrush, null, this.SkeletonPointToScreen(joint.Position), JointThickness, JointThickness);
                    AdjustSensor(); // Adjusts the sensor so that the user is with the screen 
                }
            }

            double deleteme = trackedBones / totalBones;

            fallDetect.isFall();

            // The logic behind this is that if less then 50% of the bones are being tracked successfullly and fallDetect is 
            // saying that the user's trajectory was downward then we assume the user has fallen
            if ((trackedBones / totalBones) < .5 && fallDetect.isFall() && fallDetect.StopTracking == false)
            {
                UserHasFallen(); // This activates the User-Has-Fallen protocol 
                sentinel.NotificationModule notif = new sentinel.NotificationModule();
                notif.sendNotification(" --> URGENT: " + patientName + " might have fallen. Please check on him!");
            }

        }

        
        private void UserHasFallen()
        {
            fallDetect.StopTracking = true;
            waitingResponse = true; // toggles the waiting response flag.
            CurrentQuestion = "isFall"; // Set the current question 
            SpeakThis("You might have fallen. Do you need help?"); // Activates audio queue
        }

        /// <summary>
        /// Draws a bone line between two joints
        /// </summary>
        /// <param name="skeleton">skeleton to draw bones from</param>
        /// <param name="drawingContext">drawing context to draw to</param>
        /// <param name="jointType0">joint to start drawing from</param>
        /// <param name="jointType1">joint to end drawing at</param>
        private void DrawBone(Skeleton skeleton, DrawingContext drawingContext, JointType jointType0, JointType jointType1)
        {
            Joint joint0 = skeleton.Joints[jointType0];
            Joint joint1 = skeleton.Joints[jointType1];

            // If we can't find either of these joints, exit
            if (joint0.TrackingState == JointTrackingState.NotTracked || joint1.TrackingState == JointTrackingState.NotTracked)
            {
                return;
            }

            // Don't draw if both points are inferred
            if (joint0.TrackingState == JointTrackingState.Inferred && joint1.TrackingState == JointTrackingState.Inferred)
            {
                return;
            }

            // We assume all drawn bones are inferred unless BOTH joints are tracked
            Pen drawPen = this.inferredBonePen;

            if (joint0.TrackingState == JointTrackingState.Tracked && joint1.TrackingState == JointTrackingState.Tracked)
            {
                drawPen = this.trackedBonePen;
            }

            drawingContext.DrawLine(drawPen, this.SkeletonPointToScreen(joint0.Position), this.SkeletonPointToScreen(joint1.Position));
            
            // Keeps track of where the person is on the screen 
            if (jointType1 == JointType.ShoulderCenter)
            {
                // Gets the joint coordinates in meters from the joint data 
                sentinel.PositioningCoord jointPosition = new PositioningCoord();
                jointPosition.X = joint1.Position.X;
                jointPosition.Y = joint1.Position.Y;
                jointPosition.Z = joint1.Position.Z;

                // Passes the joint data to the fall detection algorithm
                fallDetect.setJointPosition(jointPosition, DateTime.Now);

                User_Screen_Position = this.SkeletonPointToScreen(joint1.Position); 
                UserPosition.Text = "User's Position on the screen (X:Y) :  " + User_Screen_Position.X + ":" + User_Screen_Position.Y;
                fallDetect.trackUserNeckPosition(User_Screen_Position); // Tracks the trajectory of the joint to determine if the user is falling or not.
                mobilePositioning.UpdateSkelPosition(this.SkeletonPointToScreen(joint1.Position)); // Moves the kinect
            }

        }


        /// <summary>
        /// Maps a SkeletonPoint to lie within our render space and converts to Point
        /// </summary>
        /// <param name="skelpoint">point to map</param>
        /// <returns>mapped point</returns>
        private Point SkeletonPointToScreen(SkeletonPoint skelpoint)
        {
            // Convert point to depth space. 
            // We are not using depth directly, but we do want the points in our 640x480 output resolution.
            DepthImagePoint depthPoint = this.sensor.CoordinateMapper.MapSkeletonPointToDepthPoint(skelpoint, DepthImageFormat.Resolution640x480Fps30);
            int test = depthPoint.Depth;
            return new Point(depthPoint.X, depthPoint.Y);
        }



        private void AdjustSensor()
        {
            TimeSpan timeEllapsed = DateTime.Now - lastTilt;
            if (AddSensorAngle != 0 && timeEllapsed.Seconds > 1)
            {
                sensor.ElevationAngle = AddSensorAngle;
                AddSensorAngle = 0;
                lastTilt = DateTime.Now;
            }
        }

        private void TalkDamnItBTN_Click(object sender, RoutedEventArgs e)
        {
            SpeakThis(SayWhatTXT.Text);
        }


        /// <summary>
        /// This function speaks out the input string
        /// </summary>
        /// <param name="p"></param>
        private void SpeakThis(string p)
        {

            // Tells the kinect to stop listening 
            pauseListening = true;

            // Initialize a new instance of the SpeechSynthesizer.
            System.Speech.Synthesis.SpeechSynthesizer synth = new System.Speech.Synthesis.SpeechSynthesizer();

            // Configure the audio output. 
            synth.SetOutputToDefaultAudioDevice();

            // Speak a string.
            synth.SpeakAsync(p);

            // Tells the kinect to start listening once again
            synth.SpeakCompleted += synth_SpeakCompleted;

        }

        /// <summary>
        /// Tells the kinect to start listening again
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void synth_SpeakCompleted(object sender, System.Speech.Synthesis.SpeakCompletedEventArgs e)
        {
            pauseListening = false; // Puts the kinect into listening mode again 
           
            if(!waitingResponse)
                fallDetect.StopTracking = false; // Reactivates the fallDetect object

        }

        #region "Questions"

        private void isFallResponse(string resp)
        {
            if (waitingResponse)
            {
                waitingResponse = false;

                if (resp == "YES")
                {
                    SpeakThis("Stay Calm! I am getting some help.");
                    sentinel.NotificationModule notif = new sentinel.NotificationModule();
                    notif.sendNotification(patientName + " needs your help! Please come quickly.");
                }
                else
                {
                    SpeakThis("I am glad you are OK! I was just checking.");
                    sentinel.NotificationModule notif = new sentinel.NotificationModule();
                    notif.sendNotification(" False alarm! " + patientName + " is ok.");
                }

            }
        }

        #endregion


    }   
   
}
